OUTPUT = test

CXXFLAGS = -fmax-errors=8 -fno-common -Wall -Wconversion -Wdouble-promotion -Wduplicated-cond -Wduplicated-branches -Wextra -Wformat=2  -Wmisleading-indentation -Wnon-virtual-dtor -Wnull-dereference -Wold-style-cast -Wpedantic -Wshadow -Wundef -Wunused
LDFLAGS = -lSDL2main -lSDL2 -lSDL2_image

run: debug
	./$(OUTPUT)

debug:
	g++ ./src/*.cpp -I./include -std=c++11 -ggdb -g3 -Og $(CXXFLAGS) $(LDFLAGS) -o $(OUTPUT)

release:
	g++ ./src/*.cpp -I./include -std=c++11 -O3 $(LDFLAGS) -o $(OUTPUT)
