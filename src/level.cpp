/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "level.hpp"



Level::Level(Graphics& a_graphics)
{
	graphics = &a_graphics;
}



void Level::load(const char* a_filename)
{
	FILE* f = fopen(a_filename, "rb");

	for (size_t i = 0; i < LEVEL_HEIGHT; ++i)
	{
		for (size_t j = 0; j < LEVEL_WIDTH; ++j)
		{
			data[j][i] = fgetc(f);
		}
	}

	fclose(f);
}

void Level::save(const char* a_filename)
{
	FILE* f = fopen(a_filename, "wb");

	for (size_t i = 0; i < LEVEL_HEIGHT; ++i)
	{
		for (size_t j = 0; j < LEVEL_WIDTH; ++j)
		{
			fputc(data[j][i], f);
		}
	}

	fclose(f);
}

void Level::draw()
{
	for (size_t i = 0; i < LEVEL_HEIGHT; ++i)
	{
		for (size_t j = 0; j < LEVEL_WIDTH; ++j)
		{
			// WIP
			graphics->update(
				graphics->assets[data[j][i]],
				{0, 0, TILE_SIZE, TILE_SIZE},
				{j * TILE_SIZE, i * TILE_SIZE, TILE_SIZE, TILE_SIZE}
			);
		}
	}
}
