/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "graphics.hpp"

Graphics::Graphics()
{
	window = SDL_CreateWindow(
		SCREEN_TITLE,
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		0
	);

	renderer = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_ACCELERATED   |
		SDL_RENDERER_TARGETTEXTURE |
		SDL_RENDERER_PRESENTVSYNC
	);

	mainTexture = SDL_CreateTexture(
		renderer,
		SDL_PIXELFORMAT_UNKNOWN,
		SDL_TEXTUREACCESS_TARGET,
		MAIN_WIDTH,
		MAIN_HEIGHT
	);

	SDL_SetRenderTarget(renderer, mainTexture);

	IMG_Init(IMG_INIT_PNG);

	loadAssets();
}

Graphics::~Graphics()
{
	IMG_Quit();

	SDL_DestroyTexture(mainTexture);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}





void Graphics::clear(const SDL_Color& a_color)
{
	setColor(a_color);
	SDL_RenderClear(renderer);
}

void Graphics::draw()
{
	SDL_SetRenderTarget(renderer, NULL);
	SDL_RenderCopy(renderer, mainTexture, NULL, NULL);
	SDL_RenderPresent(renderer);
	SDL_SetRenderTarget(renderer, mainTexture);
}

void Graphics::update(
	SDL_Texture* a_tex,
	const SDL_Rect& a_src,
	const SDL_Rect& a_dst
){
	SDL_RenderCopy(renderer, a_tex, &a_src, &a_dst);
}

void Graphics::setColor(const SDL_Color& a_color)
{
	SDL_SetRenderDrawColor(renderer, a_color.r, a_color.g, a_color.b, a_color.a);
}



SDL_Texture* Graphics::loadImage(const char* a_filepath)
{
	SDL_Surface* sur = IMG_Load(a_filepath);

	SDL_Texture* tex;
	tex = SDL_CreateTextureFromSurface(renderer, sur);

	SDL_FreeSurface(sur);
	return tex;
}

void Graphics::loadAssets()
{
	assets[0] = loadImage("./assets/test0.png");
}





void Graphics::drawPoint(int a_x, int a_y, const SDL_Color& a_color)
{
	setColor(a_color);
	SDL_RenderDrawPoint(renderer, a_x, a_y);
}

void Graphics::drawLine(int a_x1, int a_y1, int a_x2, int a_y2,
	const SDL_Color& a_color)
{
	setColor(a_color);
	SDL_RenderDrawLine(renderer, a_x1, a_y1, a_x2, a_y2);
}

void Graphics::drawRect(const SDL_Rect& a_rect,
	const SDL_Color& a_color)
{
	setColor(a_color);
	SDL_RenderDrawRect(renderer, &a_rect);
}

void Graphics::fillRect(const SDL_Rect& a_rect,
	const SDL_Color& a_color)
{
	setColor(a_color);
	SDL_RenderFillRect(renderer, &a_rect);
}





void Graphics::drawSprite(const Sprite& a_sprite, int a_x, int a_y)
{
	update
	(
		a_sprite.tex,
		{0, 0, a_sprite.w, a_sprite.h},
		{a_x, a_y, a_sprite.w, a_sprite.h}
	);
}
