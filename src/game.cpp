/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "game.hpp"

Game::Game()
{
	running = true;
	loop();
}



void Game::loop()
{
	uint32_t frameStart;
	uint32_t deltaTime = 1;

	while (running)
	{
		frameStart = SDL_GetTicks();

		handleInput();
		update(deltaTime);
		draw();

		deltaTime = SDL_GetTicks() - frameStart;
	}
}

void Game::draw()
{
	graphics.clear();

	// ...

	// TESTING BEGIN
	test.draw(xx, yy);
	// TESTING END

	graphics.draw();
}

void Game::update(uint32_t a_deltaTime)
{
	float deltaTime = static_cast<float>(a_deltaTime) / 1000.0f;

	// ...

	// TESTING BEGIN
	xx += 4.0f * deltaTime;
	yy += 4.0f * deltaTime;
	
	test.update();
	// TESTING END
}

void Game::handleInput()
{
	static SDL_Event event;
	static const uint8_t* i_keys = SDL_GetKeyboardState(NULL);

	if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
	{
		running = false;
	}

	/* ----- CONTROLS ----- */
	if (i_keys[SDL_SCANCODE_ESCAPE]) running = false;
}
