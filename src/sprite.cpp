/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sprite.hpp"

Sprite::Sprite()
{
	tex = nullptr;
	w = 0;
	h = 0;
}

Sprite::Sprite(SDL_Texture* a_tex, int a_w, int a_h)
{
	tex = a_tex;
	w = a_w;
	h = a_h;
}

Sprite::~Sprite()
{
	SDL_DestroyTexture(tex);
	tex = nullptr; // to be sure
}
