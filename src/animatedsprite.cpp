/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "animatedsprite.hpp"

AnimatedSprite::AnimatedSprite(Graphics& a_graphics, uint8_t a_texture,
	uint32_t a_frameDuration)
{
	graphics = &a_graphics;
	texture = graphics->assets[a_texture];

	frameDuration = a_frameDuration;
	timeWhenPlayed = 0;
	index = 0;

	setupAnims();
	playAnim(0);
}



void AnimatedSprite::draw(int a_x, int a_y)
{
	SDL_Rect srcRect = animations[currentAnim][index];
	SDL_Rect dstRect = {a_x, a_y, TILE_SIZE, TILE_SIZE};

	graphics->update(texture, srcRect, dstRect);
}

void AnimatedSprite::update()
{
	if (animations[currentAnim].size() == 1) return; // idle animation



	int currentTime = SDL_GetTicks();

	if (currentTime - timeWhenPlayed >= frameDuration)
	{
		timeWhenPlayed = currentTime;

		if (index < animations[currentAnim].size() - 1)
		{
			index++;
		}
		else
		{
			index = 0;
		}
	}
}



void AnimatedSprite::setupAnims()
{
	// TEST BEGIN
	SDL_Rect tmp;

	animations[0].reserve(4);

	tmp = {0, 0, TILE_SIZE, TILE_SIZE};
	animations[0].push_back(tmp);

	tmp = {16, 0, TILE_SIZE, TILE_SIZE};
	animations[0].push_back(tmp);

	tmp = {32, 0, TILE_SIZE, TILE_SIZE};
	animations[0].push_back(tmp);

	tmp = {48, 0, TILE_SIZE, TILE_SIZE};
	animations[0].push_back(tmp);


	// TEST END

	/* EXAMPLE BEGIN

	SDL_Rect tmp;



	animations[PLAYER_ANIM_IDLE_LEFT].reserve(1);

	tmp = {0, 0, TILE_SIZE, TILE_SIZE};
	animations[PLAYER_ANIM_IDLE_LEFT].push_back(tmp);

	EXAMPLE END */
}

void AnimatedSprite::playAnim(uint8_t a_anim)
{
	if (currentAnim != a_anim)
	{
		currentAnim = a_anim;
		index = 0;
		timeWhenPlayed = SDL_GetTicks();
	}
}
