/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "defines.hpp"
#include "sprite.hpp"



class Graphics
{
private:
	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Texture* mainTexture;

	SDL_Texture* loadImage(const char*);
	void loadAssets();

public:
	SDL_Texture* assets[NUMBER_OF_ASSETS];

	Graphics();
	~Graphics();

	void clear(const SDL_Color& = COLOR_DEFAULT_BG);
	void draw();
	void update(SDL_Texture*, const SDL_Rect&, const SDL_Rect&);
	void setColor(const SDL_Color& = COLOR_DEFAULT_BG);

	void drawPoint(int, int, const SDL_Color& = COLOR_DEFAULT_FG);
	void drawLine(int, int, int, int, const SDL_Color& = COLOR_DEFAULT_FG);
	void drawRect(const SDL_Rect&, const SDL_Color& = COLOR_DEFAULT_FG);
	void fillRect(const SDL_Rect&, const SDL_Color& = COLOR_DEFAULT_FG);

	void drawSprite(const Sprite&, int, int);
};
