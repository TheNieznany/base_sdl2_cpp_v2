/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>

#include <vector>
using std::vector;

#include "defines.hpp"
#include "graphics.hpp"
#include "sprite.hpp"



class AnimatedSprite
{
private:
	Graphics* graphics;
	SDL_Texture* texture;

	uint32_t frameDuration;
	uint32_t timeWhenPlayed;

	uint8_t index;

	vector<SDL_Rect> animations[ANIM_AMOUNT];
	uint8_t currentAnim;

public:
	AnimatedSprite(Graphics&, uint8_t, uint32_t);

	void draw(int, int);
	void update();

	void setupAnims();
	void playAnim(uint8_t);
};
