/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdint>
#include <SDL2/SDL.h>

#include "graphics.hpp"
#include "animatedsprite.hpp" // XXX: testing



class Game
{
private:
	bool running;

	Graphics graphics{};

	// TESTING
	float xx = 0.0f, yy = 0.0f;
	AnimatedSprite test{graphics, 0, 250};
public:
	Game();

	void loop();

	void draw();
	void update(uint32_t);
	void handleInput();
};
