/*
 * My simple framework/engine from about mid-2021.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of base_SDL2_CPP_v2.
 *
 * base_SDL2_CPP_v2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * base_SDL2_CPP_v2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with base_SDL2_CPP_v2.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

/* ===== ----- COLORS ----- ===== */

#define COLOR_BLACK				{0x00, 0x00, 0x00, 0xFF}
#define COLOR_WHITE				{0xFF, 0xFF, 0xFF, 0xFF}
#define COLOR_RED				{0xFF, 0x00, 0x00, 0xFF}
#define COLOR_GREEN				{0x00, 0xFF, 0x00, 0xFF}
#define COLOR_BLUE				{0x00, 0x00, 0xFF, 0xFF}

#define COLOR_DEFAULT_FG		COLOR_GREEN
#define COLOR_DEFAULT_BG		COLOR_BLACK



/* ===== ----- GRAPHICS ----- ===== */

#define SCREEN_WIDTH			640
#define SCREEN_HEIGHT			480
#define SCREEN_TITLE			"base_SDL2_CPP_v2"

#define MAIN_WIDTH				320
#define MAIN_HEIGHT				240

#define TILE_SIZE				16

#define NUMBER_OF_ASSETS		1



/* ===== ----- ANIMATIONS ----- ===== */

#define ANIM_AMOUNT				1

// EXAMPLE:
//#define ANIM_PLAYER_IDLE		0
//#define ANIM_ENEMY_ATTACK		5



/* ===== ----- LEVEL ----- ===== */

#define LEVEL_WIDTH				40
#define LEVEL_HEIGHT			30
